﻿using UnityEngine;
using System.Collections;

namespace TwoDStyle
{
    public enum showedState { front, bottom, left, right, frontLeft, frontRight, bottomLeft, bottomRight, top, down }
    public abstract class TwoDStyleThreeDRenderer : TwoDStyleRenderer
    {
        showedState actualShowedState;
        
        [SerializeField]
        protected Transform orentationChecker;

        public Texture frontTexture;
        public Texture bottomTexture;
        public Texture leftTexture;
        public Texture rightTexture;

        protected Transform activeRender;
        protected Renderer activeRenderer;


        
        protected virtual void activePlate(Transform plate)
        {
            activeRender = plate;
            activeRender.gameObject.SetActive(true);
            activeRenderer = activeRender.GetChild(0).GetComponent<Renderer>();

        }

        protected virtual void SetActualShowedState(showedState state)
        {
            //Debug.Log("ActiveRenderer: " + activeRenderer + " frontTexture: " + frontTexture);
            switch (state)
            {
                case showedState.front:
                    activeRenderer.material.mainTexture = frontTexture;
                    break;
                case showedState.bottom:
                    activeRenderer.material.mainTexture = bottomTexture;
                    break;
                case showedState.left:
                    activeRenderer.material.mainTexture = leftTexture;
                    break;
                case showedState.right:
                    activeRenderer.material.mainTexture = rightTexture;
                    break;
                case showedState.frontLeft:
                    break;
                case showedState.frontRight:
                    break;
                case showedState.bottomLeft:
                    break;
                case showedState.bottomRight:
                    break;
                case showedState.top:
                    break;
                case showedState.down:
                    break;

            }
        }
        protected abstract void faceToShow(float yAngle);
        /*protected override void Update()
        {
            Base.Update();
        }*/

    }
}

