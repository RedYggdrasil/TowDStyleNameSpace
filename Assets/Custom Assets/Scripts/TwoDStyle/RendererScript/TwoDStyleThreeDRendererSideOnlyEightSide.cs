﻿using UnityEngine;
using System.Collections;
namespace TwoDStyle
{
    public class TwoDStyleThreeDRendererSideOnlyEightSide : TwoDStyleThreeDRendererSideOnly
    {

        private const float Slide = 360.0f / 16.0f;
        //public float topViewAngle = 5.0f;



        public Texture frontRightTexture;
        public Texture BottomRightTexture;
        public Texture frontLeftTexture;
        public Texture BottomLeftTexture;

        protected override void SetActualShowedState(showedState state)
        {
            //Debug.Log("ActiveRenderer: " + activeRenderer + " frontTexture: " + frontTexture);
            switch (state)
            {
                case showedState.front:
                    activeRenderer.material.mainTexture = frontTexture;
                    break;
                case showedState.bottom:
                    activeRenderer.material.mainTexture = bottomTexture;
                    break;
                case showedState.left:
                    activeRenderer.material.mainTexture = leftTexture;
                    break;
                case showedState.right:
                    activeRenderer.material.mainTexture = rightTexture;
                    break;
                case showedState.frontLeft:
                    activeRenderer.material.mainTexture = frontLeftTexture;
                    break;
                case showedState.frontRight:
                    activeRenderer.material.mainTexture = frontRightTexture;
                    break;
                case showedState.bottomLeft:
                    activeRenderer.material.mainTexture = BottomLeftTexture;
                    break;
                case showedState.bottomRight:
                    activeRenderer.material.mainTexture = BottomRightTexture;
                    break;
            }
        }


        // Update is called once per frame
        protected override void faceToShow(float yAngle)
        {
            /* // Debug Test
            if (yAngle < 0.0f)
            {
                Debug.Log("yAngle can't have a negative value in  script : " + this);
            } else */
            if (yAngle >= 15 * Slide || yAngle < Slide)
            {
                SetActualShowedState(showedState.front);
            }
            else if (yAngle < (3 * Slide))
            {
                SetActualShowedState(showedState.frontRight);
            }
            else if (yAngle < (5 * Slide))
            {
                SetActualShowedState(showedState.right);
            }
            else if (yAngle < (7 * Slide))
            {
                SetActualShowedState(showedState.bottomRight);
            }
            else if (yAngle < (9 * Slide))
            {
                SetActualShowedState(showedState.bottom);
            }
            else if (yAngle < (11 * Slide))
            {
                SetActualShowedState(showedState.bottomLeft);
            }
            else if (yAngle < (13 * Slide))
            {
                SetActualShowedState(showedState.left);
            }
            else if (yAngle < (15 * Slide))
            {
                SetActualShowedState(showedState.frontLeft);
            }
        }
    }
}